/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
        colors: {
          'bermuda': '#313131',
          'grass':  '#9bcc50',
          'poison': '#b97fc9',
          'fire':   '#fd7d24',
          'flying': '#3dc7ef',
          'water':  '#4592c4',
          'bug':    '#729f3f',
          'normal': '#a4acaf'
      }
    },
    fontFamily: {
      'sans':['Flexo-Regular','arial','sans-serif']
    }
  },
  plugins: [],
}
