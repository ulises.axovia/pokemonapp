import React from "react";

const Card = ({ pokemon }) => {
  return (
    <>
      <div className="h-auto">
        <div className="bg-white xl:mx-40 lg:mx-40 sm:mx-16 flex flex-wrap">
          {pokemon.map((poke, i) => {
            return (
              <div
                key={i}
                className="rounded w-full md:w-1/2 lg:w-1/4 px-10 pt-9 "
              >
                <div className="bg-gray-200 mx-auto my-auto rounded">
                  <img
                    className="w-full object-cover"
                    src={poke.image}
                    alt="Pokemon"
                  />
                </div>
                <div classNameName="px-6 py-1">
                  <p className="font-bold text-xl mb-2">N.°00{poke.id}</p>
                  <p className="text-gray-700 text-2xl">{poke.name}</p>
                </div>
                <div className="px-0 pt-1 pb-4">
                  {poke.types.map((tipo, index) => {
                    return (
                      <span
                        key={index}
                        className={`${poke.types[0].type.name} bg-${tipo.type.name} first-letter:rounded px-3 py-1 text-sm font-semibold text-white mr-1 mb-2`}
                      >
                        {tipo.type.name}
                      </span>
                    );
                  })}
                </div>
                <div className="bg-poison"></div>
                <div className="bg-water"></div>
                <div className="bg-bug"></div>
                <div className="bg-flying"></div>
                <div className="bg-grass"></div>
                <div className="bg-normal"></div>
                <div className="bg-fire"></div>
              </div>
            );
          })}
        </div>
      </div>
    </>
  );
};

export default Card;
