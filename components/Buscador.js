import React from 'react'

function Buscador() {
  return (
    
    <div className='flex md:mx-5 sm:mx-16 lg:mx-40 px-5 pt-9 pb-5 bg-bermuda'> 
        <div className='md:w-1/2'>
            <p className='text-white text-3xl pb-2'>Nombre o número</p>
            <input className='pt-2 rounded w-80' type="text"/>
            <p className='text-white pt-2'>¡Usa la búsqueda avanzada para encontrar Pokémon por su tipo, 
            debilidad, habilidad y demás datos!
            </p>
        </div>
        <div className='w-1/2 mx-2 rounded-sm text-white bg-green-600 my-6 p-4'>
            <h2>Busca un Pokémon por su nombre o usando su número de la Pokédex Nacional.</h2>
        </div>
    </div> //Cierre del contendor principal 8
  )
}

export default Buscador

