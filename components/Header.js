import React from "react";
import Styles from "../styles/header/header.module.css";

const Header = () => {
  return (
    <>
      <div className="xl:mx-40 lg:mx-30 md:mx-20 sm:mx-16 px-5 pt-9 pb-5 bg-white">
      <p className="text-4xl Flexo-Regular">Pokédex</p>
      </div>
    </>
  );
};

export default Header;
