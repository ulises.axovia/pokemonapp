import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Header from '../components/Header'
import Card from '../components/Card'
import Buscador from '../components/Buscador'


export async function getServerSideProps() {

  const traerPokemon = (numero) => {
    return fetch(`https://pokeapi.co/api/v2/pokemon/${numero}`)
    .then(response => response.json())
    .then(data => data)
  }
  let arrayPokemon = []
  for (let index = 1; index <= 20; index++) {
   let data = await traerPokemon(index)
   arrayPokemon.push(data)
  }

  let pokemonListo = arrayPokemon.map(pokemon => {
    return({
      id: pokemon.id,
      name: pokemon.name,
      image: pokemon.sprites.other['official-artwork'].front_default,
      types: pokemon.types
    })
  })

  return{
    props: {
      pokemonListo
    },
  }
}

export default function Home({pokemonListo}) {
  console.log("pokemonListo", pokemonListo)
  return (
    <>

    <div className="bg-[url('/images/background/background_image.png')] h-auto">
     <Header/>
     <Buscador/>
     <Card pokemon={pokemonListo}/> 
     
    </div>
    </>
  )
}
